﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CineCrud.Models
{
    public class Cine
    {
        [Key]
        public int idMov { get; set; }

        [Required(ErrorMessage = "Ingrese titulo de la pelicula")]
        [Display(Name = "Titulo")]
        public string tituloM { get; set; }

        [Required(ErrorMessage = "Ingrese genero de la pelicula")]
        [Display(Name = "Genero")]
        public string generoM { get; set; }

        [Required(ErrorMessage = "Ingrese fecha de reservacion")]
        [Display(Name = "Reservacion")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        [DataType(DataType.Date)]
        public DateTime fechaM { get; set;}

       
        [Required(ErrorMessage = "Ingrese horario")]
        [Display(Name = "Horario (24hrs)")]
        public string horario { get; set; }


        [Required(ErrorMessage = "Ingrese nombre de quien reserva")]
        [Display(Name = "Reservo: ")]
        public string reservoM { get; set; }

        [Required(ErrorMessage ="Ingrese numero de boletos")]
        [Display(Name ="Asientos:")]
        public int asientos { get; set; }

        [Required(ErrorMessage ="Forma de pago")]
        [Display(Name ="Pago")]
        public string tipoPago { get; set; }
    }
}
