﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CineCrud.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CineCrud.Controllers
{
    public class CineController : Controller
    {
        private readonly ApplicationDbContext _db;

        public CineController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Cine.ToList();
            return View(displaydata);
        }


        [HttpGet]
        public async Task<IActionResult> Index(string movSearch)
        {
            ViewData["GetCineDetails"] = movSearch;
            var empquery = from x in _db.Cine select x;
            if (!String.IsNullOrEmpty(movSearch))
            {
                empquery = empquery.Where(x => x.tituloM.Contains(movSearch) ||
                x.reservoM.Contains(movSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Cine nMov)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nMov);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nMov);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCineDetail = await _db.Cine.FindAsync(id);
            return View(getCineDetail);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCineDetail = await _db.Cine.FindAsync(id);
            return View(getCineDetail);
        }


        [HttpPost]
        public async Task<ActionResult> Edit(Cine oldRes)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldRes);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldRes);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getCineDetail = await _db.Cine.FindAsync(id);
            return View(getCineDetail);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {

            var getCineDetail = await _db.Cine.FindAsync(id);
            _db.Cine.Remove(getCineDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
