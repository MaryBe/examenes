CREATE DATABASE CIN;

USE CIN;

CREATE TABLE cine
(
idMov INT not null PRIMARY KEY IDENTITY (1,1),
tituloM nVARCHAR (150),
generoM nVarchar(15),
fechaM DATE,
horario nVarchar (8),
reservoM nVarchar (150),
asientos INT,
tipoPago nVarchar (20)
);

INSERT INTO cine (tituloM, generoM, fechaM, horario, reservoM, asientos, tipoPago)
VALUES ('Titanic','Romance','2020/07/13','14:30','Maricruz Bernal',3,'efectivo');

SELECT * FROM cine;