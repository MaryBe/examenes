﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// La información general sobre un ensamblado se controla mediante lo siguiente
// conjunto de atributos. Cambie los valores de estos atributos para modificar la información
// asociada con un ensamblado.
[assembly: AssemblyTitle("REST_CINE")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("REST_CINE")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Si configura ComVisible como falso, los tipos de este ensamblado no se hacen visibles
// para componentes COM.  Si necesita acceder a un tipo de este ensamblado desde
// COM, establezca el atributo ComVisible en True en este tipo.
[assembly: ComVisible(false)]

// El siguiente GUID sirve como ID de typelib si este proyecto se expone a COM
[assembly: Guid("d2a34ca3-7f6d-45d3-a5c3-7f5a7d2b59e6")]

// La información de versión de un ensamblado consta de los siguientes cuatro valores:
//
//      Versión principal
//      Versión secundaria
//      Número de compilación
//      Revisión
//
// Puede especificar todos los valores o puede predeterminar los números de compilación y de revisión
// mediante el carácter '*', como se muestra a continuación:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
