﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using REST_CINE.Models;

namespace REST_CINE.Controllers
{
    public class cinesController : ApiController
    {
        private CINEntities db = new CINEntities();

        // GET: api/cines
        public IQueryable<cine> Getcine()
        {
            return db.cine;
        }

        // GET: api/cines/5
        [ResponseType(typeof(cine))]
        public IHttpActionResult Getcine(int id)
        {
            cine cine = db.cine.Find(id);
            if (cine == null)
            {
                return NotFound();
            }

            return Ok(cine);
        }

        // PUT: api/cines/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putcine(int id, cine cine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cine.idMov)
            {
                return BadRequest();
            }

            db.Entry(cine).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!cineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/cines
        [ResponseType(typeof(cine))]
        public IHttpActionResult Postcine(cine cine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.cine.Add(cine);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = cine.idMov }, cine);
        }

        // DELETE: api/cines/5
        [ResponseType(typeof(cine))]
        public IHttpActionResult Deletecine(int id)
        {
            cine cine = db.cine.Find(id);
            if (cine == null)
            {
                return NotFound();
            }

            db.cine.Remove(cine);
            db.SaveChanges();

            return Ok(cine);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool cineExists(int id)
        {
            return db.cine.Count(e => e.idMov == id) > 0;
        }
    }
}