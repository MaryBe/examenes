﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_CINE.Model
{
    public partial class Cine
    {
        [Key]
        public int idMov;

        [Required(ErrorMessage = "Ingrese titulo de la pelicula")]
        public string tituloM;

        [Required(ErrorMessage = "Ingrese genero de la pelicula")]
        public string generoM;

        [Required(ErrorMessage = "Ingrese fecha de reservacion")]
        [DataType(DataType.Date)]
        public DateTime fechaM;


        [Required(ErrorMessage = "Ingrese horario")]
        public string horario;


        [Required(ErrorMessage = "Ingrese nombre de quien reserva")]
        public string reservoM;

        [Required(ErrorMessage = "Ingrese numero de boletos")]
        public Nullable<int> asientos;

        [Required(ErrorMessage = "Forma de pago")]
        public string tipoPago;

    }
}