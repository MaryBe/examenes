<?php
require ("conexion.php");

if (isset($_GET['id'])) {

$llave = $_GET['id'];

$consulta = "SELECT id_p,titulo,director,duracion,genero,estreno,sinopsis,imagen FROM pelis WHERE id_p = $llave";
$ejecuta = $conexion -> query($consulta) or die("Error de conexion" . $conexion -> error);
$datos = $ejecuta -> fetch_assoc();
} 
else{
  $datos[$id_p]='id_p';
  $datos[$titulo]='titulo';
  $datos[$director]='director';
  $datos[$duracion]='duracion';
  $datos[$genero]='genero';
  $datos[$estreno]='estreno';
  $datos[$sinopsis]='sinopsis';
  $datos[$imagen]='imagen';

}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script type="text/javascript" src="../css/jquery.js"></script>
</head>
  <head>
  	<style type="text/css">
  	body{
  		background-image: url("../img/c.jpg");
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		}

  @media screen and (max-width: 320px) 
  {
     table 
     {
      width: 100%;
      display: block;
      overflow-x: auto;
    }
  }
 	table td
     {
     	text-align: justify;
     }
	</style>
  </head>
<body >

  <header>
  <?php include("../header.php"); ?>
  </header>

<br>
<table align="center">
  <tr>
    <th rowspan="7">
    	<img class="img_detail"src="data:image/jpg;base64,<?php echo base64_encode($datos['imagen']);  ?>" >
    </th>
    <td>Titulo:</td>
    <td><?php echo $datos['titulo'];  ?></td>
  </tr>
  <tr>
  	<td>Director:</td>
  	<td><?php echo $datos['director'];  ?></td>
  </tr>
  <tr>
  	<td>Duracion:</td>
  	<td><?php echo $datos['duracion'];  ?></td>
  </tr>
  <tr>
  	<td>Genero:</td>
  	<td><?php echo $datos['genero'];  ?></td>
  </tr>
  <tr>
  	<td>Estreno:</td>
  	<td><?php echo $datos['estreno'];  ?></td>
  </tr>
  <tr>
  	<td>Sinopsis</td>
  	<td style="width:600px;"><?php echo $datos['sinopsis'];  ?></td>
  </tr>
</table>
<br>
<div align="center">
<a class="btn btn-danger" href="../index.php">Volver</a> 
</div>

</body>
<br>
<br>
<br>
<footer>
  <?php include("../footer.php"); ?>
</footer>
</html>
