<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script type="text/javascript" src="../css/jquery.js"></script>
</head>
    <style type="text/css">
    body{
      background-image: url("../img/c.jpg");
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    }
  </style>
<body>
  <header>
  <?php include("../header.php"); ?>
  </header>


<h3 align="center">NUEVO REGISTRO</h3>
<form align="center" action="nuevo.php" method="POST" enctype="multipart/form-data">

<strong>TITULO:</strong>
<input type="text" name="titulo">
<br>

<strong>DIRECTOR:</strong>
<input type="text" name="director" required>
<br>

<strong>DURACION:</strong>
<input type="number" name="duracion" required>
<br>

<strong>GENERO:</strong>
<select name="genero" required>
  <option></option>
  <option value="accion">Accion</option>
  <option value="aventura">Aventura</option>
  <option value="comedia">Comedia</option>
  <option value="terror">Terror</option>
  <option value="infantil">Infantil</option>
  <option value="romantica">Romantica</option>
</select><br>

<strong>ESTRENO:</strong>
<input type="text" name="estreno" required>
<br>

<strong>SINOPSIS:</strong>
<textarea  rows="3" cols="35" name="sinopsis" required></textarea>
<br>

<strong>IMAGEN:</strong>
<input type="file" name="imagen" required>
<br>

<input type="submit" name="Guardar"> <br><br>
<a class="btn btn-danger" href="../index.php">Volver</a> 
</form>



</body>
<br>
<br>
<br>
<footer>
  <?php include("../footer.php"); ?>
</footer>
</html>
