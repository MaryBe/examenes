<?php
require ("conexion.php");

if (isset($_GET['id'])) {

$llave = $_GET['id'];

$consulta = "SELECT id_p,titulo,director,duracion,genero,estreno,sinopsis,imagen FROM pelis WHERE id_p = $llave";
$ejecuta = $conexion -> query($consulta) or die("Error de conexion" . $conexion -> error);
$datos = $ejecuta -> fetch_assoc();
} 
else{
  $datos[$id_p]='id_p';
  $datos[$titulo]='titulo';
  $datos[$director]='director';
  $datos[$duracion]='duracion';
  $datos[$genero]='genero';
  $datos[$estreno]='estreno';
  $datos[$sinopsis]='sinopsis';
  $datos[$imagen]='imagen';

}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <script type="text/javascript" src="../css/jquery.js"></script>
</head>
    <style type="text/css">
    body{
      background-image: url("../img/c.jpg");
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    }
  </style>
<body>

  <header>
  <?php include("../header.php"); ?>
  </header>

<h3 align="center">EDITAR REGISTRO</h3>
<form align="center" action="editar.php" method="POST" enctype="multipart/form-data">

<strong>ID:  </strong>
<input type="number" name="id_p" value="<?php echo $datos['id_p'];?>" readonly>
<br>

<strong>TITULO:</strong>
<input type="text" name="titulo" value="<?php echo $datos['titulo'];?>">
<br>

<strong>DIRECTOR:</strong>
<input type="text" name="director" required value="<?php echo $datos['director'];?>">
<br>

<strong>DURACION:</strong>
<input type="number" name="duracion" required value="<?php echo $datos['duracion'];?>">
<br>

<strong>GENERO:</strong>
<select name="genero" required>
	<option><?php echo $datos['genero'];?></option>
	<option></option>
	<option value="accion">Accion</option>
	<option value="aventura">Aventura</option>
	<option value="comedia">Comedia</option>
	<option value="terror">Terror</option>
	<option value="infantil">Infantil</option>
	<option value="romantica">Romantica</option>
</select><br>

<strong>ESTRENO:</strong>
<input type="text" name="estreno" required value="<?php echo $datos['estreno'];?>">
<br>

<strong>SINOPSIS:</strong>
<textarea  rows="3" cols="35" name="sinopsis" required><?php echo $datos['sinopsis'];?></textarea>
<br>

<strong>IMAGEN:</strong>
<img src="data:image/jpg;base64,<?php echo base64_encode($datos["imagen"]); ?>">
<br>

<input type="submit" name="Guardar"> <br><br>
<a class="btn btn-danger" href="../index.php">Volver</a> 

</form>

</body>
<br>
<br>
<br>
<footer>
  <?php include("../footer.php"); ?>
</footer>
</html>
