<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <script type="text/javascript" src="css/jquery.js"></script>
  </head>

<body>
  <header>
  <?php include("header.php"); ?>
  </header>
<div>
<a class="btn btn-outline-secondary" href="php/nuevoForm.php">Agregar</a> 

  <form align="right" action="php/busqueda.php" method="POST">        
    <input name="palabra" type="search" placeholder="Busca por titulo/genero">          
     <input type="submit" value="Buscar">
  </form>
</div>



<?php
  require ("php/conexion.php");
  $consulta = "SELECT id_p,titulo,director,duracion,genero,estreno,sinopsis,imagen FROM pelis";
  $ejecuta = $conexion -> query($consulta) or die ("Error al mostrar resultados<br>". $conexion -> error);

    while ($arreglo_resultados = $ejecuta -> fetch_assoc())
    {
?>
    <tr align="center">
    <li class="catCardList">
      <div class="catCard"><img src="data:image/jpg;base64,<?php echo base64_encode($arreglo_resultados['imagen']);  ?>" >
      <div class="lowerCatCard">
      <h3><?php echo $arreglo_resultados['titulo'];  ?></h3>
      
      <p>Genero: <?php echo $arreglo_resultados['genero'];  ?></p>
      
      <p>Drigida por: <?php echo $arreglo_resultados['director'];  ?></p>

     
      <div align="center"><a href="php/editarForm.php?id=<?php echo $arreglo_resultados['id_p'];?>" class="btn btn-outline-warning">Editar</a></div>
     
      <div align="center"><a href="php/eliminar.php?id=<?php echo $arreglo_resultados['id_p'];?>" class="btn btn-outline-danger">Eliminar</a></div>      

      <div id="catCardButton" class="button"><a href="php/detalles.php?id=<?php echo $arreglo_resultados['id_p'];?>">Detalles</a></div>
      </div>
      </div>
    </li>

    </tr>
<?php
    }
?>
</table>




</body>
<br>
<br>
<br>
<footer>
	<?php include("footer.php"); ?>
</footer>

</html>
